﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FilterConfig.Startup))]
namespace FilterConfig
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
