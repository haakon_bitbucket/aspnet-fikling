﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebCam.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult OppdaterBilde()
        {
            imageData img = new imageData(GetPicturePath(), "Nordskjæret");
            return PartialView("_UpdatedPicture", model:img);
        }

        private string GetPicturePath()
        {
            return "/snapshots/snapshot.jpg";
        }
    }

    public class imageData
    {
        public imageData() { }
        public imageData(string p, string a)
        {
            path = p;
            alternate = a;
        }

        public string path { get; set; }
        public string alternate { get; set; }
    }
}