﻿using System.Web.Mvc;
using System.Web.Routing;

namespace DateContraint
{
  public class MvcApplication : System.Web.HttpApplication
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
    {
      filters.Add(new HandleErrorAttribute());
    }

    public static void RegisterRoutes(RouteCollection routes)
    {
      routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

      routes.MapRoute(
        name: "archive",
        url: "blog/archive/{datetime}",
        defaults: new { controller = "Blog", action = "Archive" },
        constraints: new { datetime = new DateConstraint() }
      );

      routes.MapRoute(
          name: "default",
          url: "{controller}/{action}/{id}",
          defaults: new { controller = "Blog", action = "Index", id = UrlParameter.Optional },
          constraints: new { eliminate = new EliminateControllerAction("Blog", "Archive") }
      );
    }

    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();

      RegisterGlobalFilters(GlobalFilters.Filters);
      RegisterRoutes(RouteTable.Routes);
    }
  }
}