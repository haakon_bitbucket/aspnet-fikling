﻿
using System;
using System.Globalization;
using System.Web;
using System.Web.Routing;

namespace DateContraint
{
  public class DateConstraint : IRouteConstraint
  {
    public bool Match(HttpContextBase httpContext, Route route, string parameterName, 
      RouteValueDictionary values, RouteDirection routeDirection)
    {
      DateTime dt;

      return DateTime.TryParseExact(values[parameterName].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
    }
  }
}