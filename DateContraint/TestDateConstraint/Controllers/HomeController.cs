﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestDateConstraint.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Archive(string dato)
        {
            var d = Request["id"];
            var r = Request;

            return new ContentResult { Content = "Received the request successfully: " + dato};
        }

        [HttpGet]
        [OutputCache(NoStore = true)]
        public ActionResult Search3(int val3)
        {
            return Content("Search3");
            //return RedirectPermanent("/home/index");
        }

        [HttpGet]
        [OutputCache(NoStore=true)]
        public ActionResult Search2(int val)
        {
            //return Json(new { Message = "meldingstekst", name = "Haakon" }, JsonRequestBehavior.AllowGet);

            var m = Server.MapPath("~/Content/Site.css");
            var f = File(m, "text/css");
            return f;

            //return RedirectToRoute("Default", new { controller = "Home", action = "About" });
            //return RedirectToAction("Index", "Home", new { val = 321 });
            //return RedirectToAction("Search3", "Home", new { val = 321 });
            //return RedirectPermanent("http://www.vg.no");
            //return Content("Search2 " + val);
        }

        [HttpGet]
        public ActionResult Search1()
        {
            return Content("Search1");
        }

    }
}