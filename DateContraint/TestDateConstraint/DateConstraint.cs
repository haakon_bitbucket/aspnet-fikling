﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Globalization;

namespace TestDateConstraint
{
    public class DateConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName,
          RouteValueDictionary values, RouteDirection routeDirection)
        {
            DateTime dt;

            return DateTime.TryParseExact(values[parameterName].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
        }
    }
}