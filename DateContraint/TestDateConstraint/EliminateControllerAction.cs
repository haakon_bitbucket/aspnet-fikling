﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace TestDateConstraint
{
    public class EliminateControllerAction : IRouteConstraint
    {
        private readonly string _controller;
        private readonly string _action;

        public EliminateControllerAction(string controller, string action)
        {
            _controller = controller;
            _action = action;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return String.Compare(values["controller"].ToString(), _controller, true) != 0 || String.Compare(values["action"].ToString(), _action, true) != 0;
        }
    }
}
