﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TestDateConstraint
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapMvcAttributeRoutes();

            //routes.MapRoute(
            //    name: "archive",
            //    url: "home/archive/{dato}",
            //    defaults: new { controller = "Home", action = "Archive" },
            //    constraints: new { dato = new DateConstraint() }
            //);

            //routes.MapRoute(
            //    name: "search1",
            //    url: "home/search",
            //    defaults: new { controller = "Home", action = "Search1" },
            //    namespaces: new[] { "TestDateConstraint" }
            //);

            //routes.Add(new Route("home/search/{val}", new StopRoutingHandler()));
            //routes.IgnoreRoute("home/search/{val}");

            routes.MapRoute(
                name: "search3",
                url: "home/search3",
                defaults: new { controller = "Home", action = "Search3", val3 = 123 },
                namespaces: new[] { "TestDateConstraint.Controllers" }
            );

            routes.MapRoute(
                name: "search2",
                url: "home/search/{val}",
                defaults: new { controller = "Home", action = "Search2", val = UrlParameter.Optional },
                constraints: new { val = @"^\d{3}$" },
                namespaces: new[] { "TestDateConstraint.Controllers" }
            );

            routes.MapRoute(
                name: "search_many",
                url: "home/search/{*val}",
                defaults: new { controller = "Home", action = "Search2", val = UrlParameter.Optional },
                namespaces: new[] { "TestDateConstraint.Controllers" }
            );

            //routes.MapRoute(
            //    name: "no_param",
            //    url: "{controller}/{action}",
            //    defaults: new { controller = "Home", action = "index"} 
            //    );

            //routes.Add(
            //    "added_route",
            //    new Route("{controller}/{action}", new MvcRouteHandler())
            //);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                constraints: new { action = new EliminateControllerAction("Home", "Search") },
                namespaces: new[] { "TestDateConstraint.Controllers" }
            );

            //routes.MapRoute(
            //    name: "archive",
            //    url: "home/archive/{id}",
            //    defaults: new { controller = "Home", action = "Archive", id = UrlParameter.Optional }
            //);

        }
    }
}
