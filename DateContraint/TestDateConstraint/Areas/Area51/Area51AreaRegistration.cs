﻿using System.Web.Mvc;

namespace TestDateConstraint.Areas.Area51
{
    public class Area51AreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Area51";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                name: "area51_search2",
                url: "area51/home/search/{val}",
                defaults: new { controller = "MyHome", action = "Search2", val = UrlParameter.Optional }
            );

            context.MapRoute(
                "Area51_default",
                "Area51/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}