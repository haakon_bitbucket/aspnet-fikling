﻿using System.Web.Mvc;

namespace TestDateConstraint.Areas.Area9
{
    public class Area9AreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Area9";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "area9_search2",
                url: "area9/home/search/{val}",
                defaults: new { controller = "MyHome", action = "Search2", val = UrlParameter.Optional }
            );

            context.MapRoute(
                "Area9_default",
                "Area9/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}