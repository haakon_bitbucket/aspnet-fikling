﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestDateConstraint.Areas.Area9.Controllers
{
    public class MyHomeController : Controller
    {
        // GET: Area9/Home
        public ActionResult Index()
        {
            return Content("Area9::Index");
        }

        [HttpGet]
        public ActionResult Search2(int val)
        {
            return Content("Area9::Search " + val);
        }

        [HttpGet]
        public ActionResult Search1()
        {
            return Content("Area9::Search");
        }
    }
}